import { Option } from "../types";
import DeterminersSection from "../determiner/DeterminersSection";
import { DecisionDeterminer } from "../determiner/types";
import { DeterminerType } from "./types";
import useDependentState from "../common/use-dependent-state";
import EditIcon from "../icon/edit.svg";
interface OptionSectionProps extends Option {
  position: number;
  onChangeTitle: (title: string) => void;
  onChangeDeterminer: (
    position: number,
    type: DeterminerType,
    determiner: DecisionDeterminer
  ) => void;
}
export default function OptionSection(props: OptionSectionProps) {
  const { title, pros, cons, position, onChangeTitle, onChangeDeterminer } =
    props;
  const [localTitle, setLocalTitle] = useDependentState(title);
  console.log({ localTitle });
  return (
    <>
      <div className="option-header">
        <h1 className="option-header-number">{`Option #${position}: `}</h1>
        <span className="option-header-title-with-edit">
          <input
            className="option-header-title"
            type="text"
            value={localTitle}
            onChange={(e) => setLocalTitle(e.target.value)}
            onBlur={() => onChangeTitle(localTitle)}
          />
          <img
            className="edit-icon edit-icon-input"
            src={EditIcon}
            alt="Edit"
          />
        </span>
      </div>
      <DeterminersSection
        header="Pros"
        determiners={pros}
        onChangeDeterminer={(index, determiner) =>
          onChangeDeterminer(index, "Pro", determiner)
        }
      />
      <DeterminersSection
        header="Cons"
        determiners={cons}
        onChangeDeterminer={(index, determiner) =>
          onChangeDeterminer(index, "Con", determiner)
        }
      />
    </>
  );
}
