import React, { useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import "./option/option.css";
import DeterminersSection from "./determiner/DeterminersSection";
import { DecisionDeterminer } from "./determiner/types";
import OptionSection from "./option/OptionSection";
import { Option } from "./types";
import { DeterminerType } from "./option/types";
const defaultOptions: Option[] = [
  {
    title: "Do this",
    pros: [{ description: "Great reason", weight: 8 }],
    cons: [{ description: "Average reason", weight: 5 }],
  },
  {
    title: "Do that",
    pros: [{ description: "Another great reason", weight: 10 }],
    cons: [{ description: "Meh", weight: 3 }],
  },
];

function App() {
  const [options, setOptions] = useState<Option[]>(defaultOptions);

  const updateTitle = (optionIndex: number, title: string) =>
    setOptions((options) =>
      replaceWithUpdates(options, optionIndex, { title })
    );

  const updateDeterminer = (
    optionIndex: number,
    determinerType: DeterminerType,
    determinerIndex: number,
    determiner: DecisionDeterminer
  ) => {
    const determinerList = determinerType === "Pro" ? "pros" : "cons";

    setOptions((options) => {
      const updatedDeteminers = replaceWithUpdates(
        options[optionIndex][determinerList],
        determinerIndex,
        determiner
      );

      return replaceWithUpdates(options, optionIndex, {
        [determinerList]: updatedDeteminers,
      });
    });
  };
  return (
    <div>
      {options.map((option, i) => {
        const { title, pros, cons } = option;
        return (
          <OptionSection
            position={i + 1}
            title={title}
            key={title}
            pros={pros}
            cons={cons}
            onChangeTitle={(title) => updateTitle(i, title)}
            onChangeDeterminer={(determinerIndex, type, determiner) =>
              updateDeterminer(i, type, determinerIndex, determiner)
            }
          />
        );
      })}
    </div>
  );
}

export default App;

function replaceWithUpdates<T>(
  list: T[],
  position: number,
  updates: Partial<T>
): T[] {
  const copy = [...list];
  copy.splice(position, 1, { ...list[position], ...updates });
  return copy;
}
