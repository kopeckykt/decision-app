import { DecisionDeterminer } from "./determiner/types";

export interface Option {
  title: string;
  pros: DecisionDeterminer[];
  cons: DecisionDeterminer[];
}

export interface Decision {
  title: string;
  options: Option[];
}
