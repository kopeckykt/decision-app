import React, { useEffect, useState } from "react";

export default function useDependentState<T>(
  parentValue: T
): [T, React.Dispatch<T>] {
  const [childValue, setChildValue] = useState<T>(parentValue);

  useEffect(() => {
    setChildValue(parentValue);
  }, [parentValue]);

  return [childValue, setChildValue];
}
