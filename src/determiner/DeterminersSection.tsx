import { useState } from "react";
import useDependentState from "../common/use-dependent-state";
import { DecisionDeterminer, Weight } from "./types";

interface DeterminersSectionProps {
  header: string;
  determiners: DecisionDeterminer[];
  onChangeDeterminer: (
    position: number,
    determiner: DecisionDeterminer
  ) => void;
}

export default function DeterminersSection(props: DeterminersSectionProps) {
  const { header, determiners, onChangeDeterminer } = props;

  return (
    <>
      <h2>{header}</h2>
      {determiners.map(({ description, weight }, i) => (
        <li key={`${description}-${weight}`}>
          <Determiner
            description={description}
            weight={weight}
            onChangeWeight={(weight: Weight) =>
              onChangeDeterminer(i, { description, weight })
            }
            onChangeDescription={(description: string) =>
              onChangeDeterminer(i, { description, weight })
            }
          />
        </li>
      ))}
    </>
  );
}

interface DeterminerProps extends DecisionDeterminer {
  onChangeDescription: (description: string) => void;
  onChangeWeight: (weight: Weight) => void;
}
function Determiner(props: DeterminerProps) {
  const { description, weight, onChangeDescription, onChangeWeight } = props;

  const [localDescription, setLocalDescription] =
    useDependentState(description);

  const [localWeight, setLocalWeight] = useDependentState(weight);

  const handleWeightChange = (e: any) => {
    const weight = Number(e.target.value) as Weight;
    if (!isNaN(weight)) {
      setLocalWeight(weight);
    }
  };
  return (
    <>
      <textarea
        value={localDescription}
        onChange={(e) => setLocalDescription(e.target.value)}
        onBlur={() => onChangeDescription(localDescription)}
      />
      <input
        type="text"
        value={localWeight}
        onChange={handleWeightChange}
        onBlur={() => onChangeWeight(localWeight)}
      />
    </>
  );
}
